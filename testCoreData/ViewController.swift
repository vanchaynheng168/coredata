//
//  ViewController.swift
//  testCoreData
//
//  Created by BTB_011 on 25/11/20.
//

import UIKit

class ViewController: UIViewController {

    var items: [Person]?
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    @IBOutlet weak var table: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    func FetchPeople() {
        do{
            self.items = try context.fetch(Person.fetchRequest())
            DispatchQueue.main.async {
                self.table.reloadData()
            }
            
        }catch{
            
        }
    }
    @IBAction func addItem(_ sender: Any) {
        let alert = UIAlertController(title: "Add Person", message: "ok", preferredStyle: .alert)
        alert.addTextField()
    }
}
extension ViewController: UITableViewDelegate{
    
}
extension ViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let person = items![indexPath.row]
        cell.textLabel?.text = person.name
        return cell
    }
    
    
}

